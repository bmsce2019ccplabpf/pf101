#include<stdio.h>
#include<math.h>
void input_point(float *x,float *y)
{
	printf("enter a point (x,y) \n");
	scanf("%f%f",&x,&y);
}
float distance(float x1,float x2,float y1,float y2)
{
	float d;
	d=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
	return d;
}
void output(float x1,float x2,float y1,float y2,float d)
{
	printf("the distance between %f,%f and %f,%f is %f",x1,y1,x2,y2,d);
}
int main()
{
	float x1,y1,x2,y2;
	input_point(&x1,&y1);
	input_point(&x2,&y2);
	float d=distance(x1,x2,y1,y2);
	output(x1,x2,y1,y2,d);
	return 0;
}